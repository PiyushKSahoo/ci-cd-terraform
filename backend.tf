terraform {
  backend "s3" {
    bucket = "mystatebucketterraform"
    key    = "state"
    region = "us-east-1"
    dynamodb_table = "backend"
  }
}
